BINARY_NAME=main

build:
	echo "Compiling for every OS and Platform"
	mkdir -p out/bin
	GOARCH=amd64 GOOS=darwin go build -o out/bin/${BINARY_NAME}-darwin main.go
	GOARCH=amd64 GOOS=linux go build -o out/bin/${BINARY_NAME}-linux main.go
	GOARCH=amd64 GOOS=windows go build -o out/bin/${BINARY_NAME}-windows main.go

test:
	go test -v ./...

clean: ## Clean the project:
	go clean
	rm -rf out/bin/${BINARY_NAME}-darwin
	rm -rf out/bin/${BINARY_NAME}-linux
	rm -rf out/bin/${BINARY_NAME}-windows